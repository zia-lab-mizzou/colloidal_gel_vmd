# functions.tcl
#
# Includes:
#   clips_off: Turn off all clipping planes, in all representations and molecules.
#   default_start: Apply my own default options (after loading as points).
#   get_num_types mol: Count until you find a type with selection size zero
#   graphics_clear: Clear all graphics.
#   gray_spheres mol: Represent molecule as monodisperse gray spheres.
#   gray_spheres_poly mol diams: Represent mol as gray spheres with given diams
#   HDF5_to_tga: Use external functions and HDF5 dump to output as .tga images.  Automatically animate.
#   set_radii: Change radii in mol to radius for all VDW reps
#   load_user: Read from list of files to set user values for each atom in system.
#   mol_circle mol center normal radius color: Draw a circle with given center, normal vector, radius, and color, based on coordinates of given molecule.
#   repclear mol: Clear all represoentations on given molecule.
#   subtract_drift: Subtract off mean velocity from all atoms (based on momentum)
#   user_poly: set to polydisperse vdW with given material
#   veccolin vec rep tol: Test if vectors are colinear within tolerance.
#   vecproj v u: Project vector v onto u.
#   view_slab molid axis thickness:
#
# Required:
#   trj_dump executable in fpostproc
#   ffmpeg

# gray_spheres_ploly

# Turn off all clipping planes, in all representations and molecules.
proc clips_off {} {
    puts "Turning off all clipping planes"
    set mols [molinfo list]
    for {set i 0} {$i < [llength $mols]} {incr i} {
	set mol [lindex $mols $i]
	for {set rep 0} {$rep < [molinfo $mol get numreps]} {incr rep} {
	    for {set j 0} {$j < 6} {incr j} {
		mol clipplane status $j $rep $mol 0
	    }
	}
    }
}

proc default_start {} {
    display resize 1000 1000
    pbc wrap -all
    display projection orthographic
    display depthcue off
    scale to 0.02
}

# From Axel Kohlmeyer
proc gen_render \
    {{start 0} {end -1} {step 1} {prog POV3} {format "frame-%05d.pov"}} {
	
        if {$end < 0} {set end [expr [molinfo [molinfo top] get numframes]-1]}
        
        puts "writing input for rendering with $prog with format $format"
        puts "from step $start to step $end with delta $step"
        for {set i $start} {$i <= $end} {incr i $step} {
            animate goto $i
            render $prog [format $format $i]
        }
}


proc graphicsclear {} {
    puts "Clearing all graphics"
    set mols [molinfo list]
    for {set i 0} {$i < [llength $mols]} {incr i} {
	set mol [lindex $mols $i]
	set graphs [graphics $mol list]
	for {set j 0} {$j < [llength $graphs]} {incr j} {
	    graphics $mol delete [lindex $graphs $j]
	}
    }
}

# NOTE: stops once we reach a type selection of size 0 from type 1
proc get_num_types {mol} {
    for {set i 1} {1} {incr i} {
	set sel [atomselect $mol "type $i"]
	if {[$sel num] == 0} {
	    return [expr $i-1]
	}
    }
}

proc gray_spheres {mol mat} {
    puts "Representing as monodisperse gray spheres"
    repclear $mol
    mol color ColorID 2
    mol representation VDW 0.3 15
    mol selection all
    mol material $mat
    mol addrep $mol
}

proc gray_spheres_poly {mol diams mat} {
    set numtypes [get_num_types $mol]
    if {[llength $diams] != $numtypes} {
	puts "Need to supply values for $numtypes types"
	return
    }
    repclear $mol
    for {set i 1} {$i <= $numtypes} {incr i} {
	mol addrep $mol
	set rep_number [expr $i-1]
	set diam [expr 0.33*[lindex $diams [expr $i-1]]]
	mol modselect $rep_number $mol "type $i"
	mol modstyle $rep_number $mol VDW $diam 12
	mol modmaterial $rep_number $mol $mat
	mol modcolor $rep_number $mol ColorID 2
    }
}

proc HDF5_to_tga {HDF5_path tstart tdur tinc trj_set_tcl frame_set_tcl} {
    set numsnaps [expr int($tdur/$tinc)+1]
    set digits [expr int(log10($numsnaps-1))+1]
    set first_snap 1
    for {set i 0} {$i < $numsnaps} {incr i} {
	set timestep [expr $tstart+$i*$tinc]
	puts "timestep $timestep"
	exec trj_dump $HDF5_path $timestep yes no
	set snap ${timestep}.lammpstrj
	if {$first_snap} {
	    puts "first step $i"
	    set mol [mol new $snap]  
	    source $trj_set_tcl
	    set first_snap 0
	} else {
	    puts "not first step but $i"
	    animate read lammpstrj $snap $mol
	    animate delete beg 0 end 0 skip 0 $mol
	}
	source $frame_set_tcl
	set img_name [format "%0${digits}d.tga" $i]
	render TachyonInternal $img_name
    }
    exec ffmpeg -r 25 -i %0${digits}d.tga out.avi
}

# Sequentially load a series of files that give user values for all atoms in molecule.
# One for each frame.
# The procedure sets the user value using the ordered values it finds in the file.
proc load_user {mol path_list} {
    set numframes [molinfo $mol get numframes]
    set numatoms [molinfo $mol get numatoms]
    set sel [atomselect $mol all]

    for {set i 0} {$i < $numframes} {incr i} {
        animate goto $i
        $sel frame $i
        puts "Setting User data for frame [$sel frame]"

        # Edit below to go through different types of files.
        # The file names must be pre-formatted (e.g. '0.nc_list')
        set nc_file [open [lindex $path_list $i] r]

        # Make a list with elements equal to the user field.
        set user_list ""
        for {set j 0} {$j < $numatoms} {incr j} {
            gets $nc_file line
            lappend user_list [lindex $line 1]
        }

        # Set the user field to elements in this list for the selected frame.
        $sel set user $user_list
    }
}

proc load_user_dumpfile {mol path fieldname} {
    set numframes [molinfo $mol get numframes]
    set numatoms [molinfo $mol get numatoms]
    set sel [atomselect $mol all]

    set dump_file [open $path r]
    set firstflag 1

    for {set i 0} {$i < $numframes} {incr i} {
	animate goto $i
	$sel frame $i
	#puts "Setting User data to $fieldname for frame [$sel frame]"

	set user_list ""

	# Skip the header
	for {set j 0} {$j < 9} {incr j} {
	    gets $dump_file line
	}
	if {$firstflag} {
	    set field_index -1
	    for {set j 0} {$j < [llength $line]} {incr j} {
		if {[string equal [lindex $line $j] $fieldname]} {
		    set field_index [expr $j-2]
		    break
		}
	    }
	    if {$field_index == -1} {
		puts "Error: could not find field $fieldname in dump"
		return
	    }
	    set firstflag 0
	}

	# Read the values
	for {set j 0} {$j < $numatoms} {incr j} {
	    gets $dump_file line
	    lappend user_list [lindex $line $field_index]
	}

	# Set the User field to elements in this list for selected frame
	$sel set user $user_list
    }
}

# Draw a circle with given center, normal vector, and radius, based on coordinates of given molecule.
# If the normal vector is colinear with x or y, use Cartesian coordinate system to draw circle.  If not, use Gram-Schmidt to get the circle.  IMPROVE COMMENT!
# Normalize the input normal vector
proc mol_circle {mol center normal radius color} {
    variable M_PI
    set num_segments 1000
    set width 10
    set tol 0.0001
    puts $color
    graphics $mol color $color
    set normal [vecnorm $normal]
    if {[veccolin $normal {1 0 0} $tol]} {
	set plane_vec_1 {0 1 0}
	set plane_vec_2 {0 0 1}
    } elseif {[veccolin $normal {0 1 0} $tol]} {
	set plane_vec_1 {1 0 0}
	set plane_vec_2 {0 0 1}
    } else {
	set plane_vec_1 [vecsub {1 0 0} [vecproj {1 0 0} $normal]]
	set plane_vec_2 [vecsub {0 1 0} [vecadd [vecproj {0 1 0} $normal] [vecproj {0 1 0} $plane_vec_1]]]
    }
    set old_point [vecadd $center [vecscale $radius $plane_vec_2]]
    for {set i 1} {$i <= $num_segments} {incr i} {
	set angle [expr $i*2.0*$M_PI/$num_segments]
	set delta_1 [vecscale [expr $radius*sin($angle)] $plane_vec_1]
	set delta_2 [vecscale [expr $radius*cos($angle)] $plane_vec_2]
	set delta [vecadd $delta_1 $delta_2]
	set new_point [vecadd $center $delta]
	graphics $mol line $old_point $new_point width $width
	set old_point $new_point
    }
}

# Clear all representations for given molecule.
proc repclear {mol} {
    puts "Clearing all representations for molecule $mol"
    set numreps [molinfo $mol get numreps]
    for {set i 0} {$i < $numreps} {incr i} {
        set rep [expr $numreps-$i-1]
	mol delrep $rep $mol
    }
}

proc set_masses {mol masses} {
    set numtypes [get_num_types mol]
    if {[llength $masses] != $numtypes} {
	puts "Error: masses list length not equal to number of types"
	return
    }
    for {set i 1} {$i <= $numtypes} {incr i} {
	set sel [atomselect $mol "type $i"]
	$sel set mass [lindex $masses [expr $i-1]]
    }
}

#proc set_sphere_scale {mol radius
#proc hide_sphere {mol}


# Subtract drift from all atoms
# Need to specify masses for all types (dump files don't contain)
#proc subtract_drift {mol} {
#    set numtypes [get_num_types mol]
#    puts "Subtracting drift"
#    set sel [atomselect mol all]

# Set to given material
proc user_poly {mol diams material scale_min scale_max} {
    set numtypes [get_num_types $mol]
    if {[llength $diams] != $numtypes} {
	puts "Need to supply values for $numtypes types"
	return
    }
    repclear $mol
    for {set i 1} {$i <= $numtypes} {incr i} {
	mol addrep $mol
	set rep_number [expr $i-1]
	set diam [expr 0.33*[lindex $diams [expr $i-1]]]
	mol modselect $rep_number $mol "type $i"
	mol modstyle $rep_number $mol VDW $diam 12
	mol modmaterial $rep_number $mol $material
	mol modcolor $rep_number $mol User
        mol colupdate $rep_number $mol on
	mol selupdate $rep_number $mol on
	mol scaleminmax $mol $rep_number $scale_min $scale_max
    }
}
    

# See if given vector is colinear with reference vector within given tolerance.
# Assuming pass by value.
proc veccolin {vec ref tol} {
  variable M_PI
    set vec [vecscale 1.0 $vec]
    set ref [vecscale 1.0 $ref]
    set vec_length [veclength $vec]
    set ref_length [veclength $ref]
    set dot_product [vecdot $vec $ref]
    set angle [expr abs(acos($dot_product/$vec_length/$ref_length))]
    if {$angle > [expr $M_PI/2.0]} {set angle [expr $M_PI-$angle]}
    if {$angle < $tol} {
	puts "Angle within tolerance"
	return 1
    } else {
	return 0
    }
}

# Projection of vector v onto vector u
proc vecproj {v u} {
    set v [vecscale 1.0 $v]
    set u [vecscale 1.0 $u]
    return [vecscale [expr [vecdot $v $u]/[vecdot $u $u]] $u]
}

# Color by Nc using velocity field as loaded by fppvmd
proc vel_nc_poly_cut {mol diams material scale_min scale_max axis shave} {
    set numtypes [get_num_types $mol]
    if {[llength $diams] != $numtypes} {
	puts "Need to supply values for $numtypes types"
	return
    }
    repclear $mol
    for {set i 1} {$i <= $numtypes} {incr i} {
	mol addrep $mol
	set rep_number [expr $i-1]
	set diam [expr 0.33*[lindex $diams [expr $i-1]]]
	if {$axis == 0} {
	    set saxis x
	} elseif {$axis == 1} {
	    set saxis y
	} elseif {$axis == 2} {
	    set saxis z
	} else {
	    puts "axis needs to be 0, 1, or 2"
	    return
	}
	set cell [molinfo $mol get {a b c}]
	set plane [expr $shave*[lindex $cell $axis]]
	mol modselect $rep_number $mol "type $i && $saxis > $plane"
	mol modstyle $rep_number $mol VDW $diam 12
	mol modmaterial $rep_number $mol $material
	mol modcolor $rep_number $mol Velocity
        mol colupdate $rep_number $mol on
	mol selupdate $rep_number $mol on
	mol scaleminmax $mol $rep_number $scale_min $scale_max
    }
}

# slight variation of vel_nc_poly_cut,
#   so that it can ignore certain particles (set radius to be small before this function) 
proc vel_q6_poly_cut {mol diams material scale_min scale_max axis shave} {
    set numtypes [get_num_types $mol]
    if {[llength $diams] != $numtypes} {
	puts "Need to supply values for $numtypes types"
	return
    }
    # repclear $mol
    for {set i 1} {$i <= $numtypes} {incr i} {
	mol addrep $mol
	set rep_number [expr $i-1]
	set diam [expr 0.33*[lindex $diams [expr $i-1]]]
	if {$axis == 0} {
	    set saxis x
	} elseif {$axis == 1} {
	    set saxis y
	} elseif {$axis == 2} {
	    set saxis z
	} else {
	    puts "axis needs to be 0, 1, or 2"
	    return
	}
	set cell [molinfo $mol get {a b c}]
	set plane [expr $shave*[lindex $cell $axis]]
	mol modselect $rep_number $mol "type $i && $saxis > $plane"
	mol modstyle $rep_number $mol VDW $diam 12
	mol modmaterial $rep_number $mol $material
	mol modcolor $rep_number $mol Velocity
        mol colupdate $rep_number $mol on
	mol selupdate $rep_number $mol on
	mol scaleminmax $mol $rep_number $scale_min $scale_max
    }
}
# Color by Nc using velocity field as loaded by fppvmd
#
proc track_particles {mol diams material scale_min scale_max axis shave ntrack ids} {
    set numtypes [get_num_types $mol]
    if {[llength $diams] != $numtypes} {
	puts "Need to supply values for $numtypes types"
	return
    }
    if {[llength $ids] != $ntrack} {
	puts "Need to supply atom ids for $ntrack particles"
	return
    }
    repclear $mol
#    for {set i 1} {$i <= $numtypes} {incr i} {
#	mol addrep $mol
#	set rep_number [expr $i-1]
#	set diam [expr 0.33*[lindex $diams [expr $i-1]]]
#	if {$axis == 0} {
#	    set saxis x
#	} elseif {$axis == 1} {
#	    set saxis y
#	} elseif {$axis == 2} {
#	    set saxis z
#	} else {
#	    puts "axis needs to be 0, 1, or 2"
#	    return
#	}
#	set cell [molinfo $mol get {a b c}]
#	set plane [expr $shave*[lindex $cell $axis]]
#	mol modselect $rep_number $mol "type $i && $saxis > $plane"
#	mol modstyle $rep_number $mol VDW $diam 12
#	mol modmaterial $rep_number $mol $material
#        material change opacity $material 0
#	mol modcolor $rep_number $mol Velocity
#        mol colupdate $rep_number $mol on
#	mol selupdate $rep_number $mol on
#	mol scaleminmax $mol $rep_number $scale_min $scale_max
#    }

    for {set j 1} {$j <= $ntrack} {incr j} {
	mol addrep $mol
	set rep_number [expr -1+$j]
        set sel [atomselect $mol "serial [lindex $ids [expr $j-1]]"] 
	set diam [expr 0.33*[lindex $diams [expr [$sel get type]-1]]]
	if {$axis == 0} {
	    set saxis x
	} elseif {$axis == 1} {
	    set saxis y
	} elseif {$axis == 2} {
	    set saxis z
	} else {
	    puts "axis needs to be 0, 1, or 2"
	    return
	}
	set cell [molinfo $mol get {a b c}]
	set plane [expr $shave*[lindex $cell $axis]]
	mol modselect $rep_number $mol "serial [lindex $ids [expr $j-1]]"
	mol modstyle $rep_number $mol VDW $diam 12
	mol modmaterial $rep_number $mol $material
	mol modcolor $rep_number $mol Velocity
        mol colupdate $rep_number $mol on
	mol selupdate $rep_number $mol on
	mol scaleminmax $mol $rep_number $scale_min $scale_max
    }
}

# View slab, normal to given axis, with given thickness.
# Slab is centered at the center of the box listed in molinfo.
# Note: using clipplanes 1 and 2, because 0 is buggy.
proc view_slab {molid axis thickness} {
    switch $axis {
	0 {set letter "x"}
	1 {set letter "y"}
	2 {set letter "z"}
	default {puts "Bad axis for slab"}
    }
    clips_off
    puts "Viewing slab with thickness $thickness normal to $letter"
    set cell [molinfo $molid get {a b c}]
    set center [vecscale $cell 0.5]
    set displacement_1 [lreplace [veczero] $axis $axis [expr -$thickness/2.0]]
    set displacement_2 [lreplace [veczero] $axis $axis [expr +$thickness/2.0]]
    set center_1 [vecadd $center $displacement_1]
    set center_2 [vecadd $center $displacement_2]
    set numreps [molinfo $molid get numreps]
    for {set repid 0} {$repid < $numreps} {incr repid} {
	mol clipplane center 1 $repid $molid $center_1
	mol clipplane center 2 $repid $molid $center_2
	mol clipplane normal 1 $repid $molid [lreplace [veczero] $axis $axis +1]
	mol clipplane normal 2 $repid $molid [lreplace [veczero] $axis $axis -1]
	mol clipplane status 1 $repid $molid 1
	mol clipplane status 2 $repid $molid 1
    }
}

#VMD  --- start of VMD description block
#Name:
# Trajectory path
#Synopsis:
# Draws the path of the center of mass of a selection through an animation
#Version:
# 1.2
#Uses VMD version:
# 1.8
#Ease of use:
# 2
#Procedures:
# <li>trajectory_path selection {color blue} {linewidth 1} {update 0}
# -- follows the center of mass of the given selection.  
# 'color' is a solid color, or "scale" for a color scale,
# 'update' toggles calling "$selection update", and 'linewidth' is 
# the width of the line drawn.
#Description:
# For each step in the animation, the center of mass of the selection is
# calculated.  A line connecting the successive center of mass coordinates
# is then added to the molecule id of the selection. 
# The color is a solid color (default is blue) or they are mapped to the 
# color scale from lowest (= first trajectory frame) to highest (= last 
# trajectory frame).
# The third argument decides (0 = no, 1 = yes) whether the selection
# is updated during the course of following the C.O.M. (default is no).
# The fourth argument allows to specify the width of the line (default 1).
# The procedure returns the graphics ids of the drawn objects.
#Example:
# <pre>
# set water [atomselect top "resid 5243"]
# trajectory_path $water scale
#
# # follow solvation shell around an atom.
# set solv  [atomselect top "water and exwithin 3.8 of index 199"]
# trajectory_path $solv yellow 3 1
#Files: 
# <a href="trajectory_path.vmd">trajectory_path</a>
#Author: 
# Andrew Dalke &lt;dalke@ks.uiuc.edu&gt;
# Axel Kohlmeyer &lt;axel.kohlmeyer@rub.de&gt; (linewidth/update)
#\VMD  --- end of block

proc trajectory_path {selection {color blue} {linewidth 1} {update 0}} {

 # save the current selection frame number and get the molecule id.
 set sel_frame [$selection frame]
 set gr_mol [$selection molindex]

 # make the list of coordinates
 set num_frames [molinfo $gr_mol get numframes]
 set coords {}
 for {set i 0} {$i < $num_frames} {incr i} {
   $selection frame $i
   if {$update} { $selection update }
   # compute the center of mass and save it on the list
   lappend coords [measure center $selection weight mass]
 }
 ##### now make the graphics and store the respective graphic ids in a list.
 set gr_list {}
 set coords [lassign $coords prev]

 # use the color scale?
 if {$color == "scale"} {
   set count 0
   incr num_frames
   foreach coord $coords {
     set color [expr [colorinfo num] + int([colorinfo max] * ($count + 0.0) / ($num_frames + 0.0))]
     graphics $gr_mol color $color
     lappend gr_list [graphics $gr_mol line $prev $coord width $linewidth]
     set prev $coord
     incr count
   }
 } else {
   # constant color
   graphics $gr_mol color $color
   foreach coord $coords {
     lappend gr_list [graphics $gr_mol line $prev $coord width $linewidth]
     set prev $coord
   }
 }

 # return the selection to its original state
 $selection frame $sel_frame
 if {$update} {$selection update}
 return $gr_list
}

