# Load the pressure in, first getting the correct number of frames.
set numframes [molinfo top get numframes]
set numatoms [molinfo top get numatoms]
#set dumpfile [open [molinfo top get filename] r]
set sel [atomselect top all]

for {set i 0} {$i < $numframes} {incr i} {
  animate goto $i
  $sel frame $i
  puts "Setting User data to Nc for frame [$sel frame]"

  # Edit below to go through different types of files.
  # The file names must be pre-formatted (e.g. '0200.nc_list')
  set nc_file [open [format "%02d.nc_list" [expr $i]] r]

  # Make a list with elements equal to the user field.
  set user_list ""
  for {set j 0} {$j < $numatoms} {incr j} {
    gets $nc_file line
    lappend user_list [lindex $line 1]
  }

  # Set the user field to elements in this list for the selected frame.
  $sel set user $user_list

}

# ADD CODE FOR REPRESENTATIONS AND UPDATING SELECTION

#  for {set j 0} {$j < 9} {incr j} {
    
#  for {set j 0} {$j < $numatoms} {incr j} {
    


# Load in the pressure data from the same trajectory file.



# Make an atom selection, set the "User" fields for all atoms
# in the selection.

#set sel [atomselect top "all"]
#for {set i 0} {$i < $numframes} {incr i} {
#  animate goto $i
#  $sel frame $i
#  puts "Setting User data for frame [$sel frame] ..."
#  $sel set user $i
#}
#$sel delete

# Change the "color by" and "trajectory" tab settings to new colors,
# and start it animating.
#mol delrep 0 top; # first is rep, second molecule
#mol rep VDW 0.3 12
#mol addrep top; # 0 for molecule
#mol modcolor 0 top User; # first 0 rep_number, second molecule_number
#mol colupdate 0 top 1; # first 0 rep_number, second molecule_number
#mol scaleminmax top 0 0.0 $numframes; # first 0 molecule_number, second rep_number
#animate forward
