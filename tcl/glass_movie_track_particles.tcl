# NOTE: fppvmd and batch need to be input

# Parse command-line arguments
set ntrack 2
if {$argc != 11} {
    puts "USAGE: vmd -args FPPVMD BATCH SIZES -dispdev none -eofexit < glass_movie_track_particles.tcl"
    exit 1
}
set fppvmd [lindex $argv 0]
set batch [lindex $argv 1]

package require pbctools

set mat AOShiny
set diams "[lindex $argv 2] [lindex $argv 3] [lindex $argv 4] [lindex $argv 5] [lindex $argv 6]"
#"0.9 0.95 1.0 1.05 1.1"
set ids "37685 4156"
set scale_min 0
set scale_max 14
set rend TachyonInternal
set fmt [concat [format "%05d" $batch]-%05d.tga]

mol new $fppvmd waitfor all

light 0 off
light 1 on
display ambientocclusion on
display aoambient 0.80
display aodirect 0.60
display shadows on
display resize 1000 1000
display projection perspective
display depthcue off
axes location off

pbc wrap -all -orthorhombic -center bb
# once more for good luck
pbc wrap -all -orthorhombic -center bb
pbc box -center bb
display resetview
rotate y by 45
rotate x by 40
display distance 0
display height 2.8
translate by 0 0.2 0
#molinfo top get {center center_matrix rotate_matrix scale_matrix global_matrix view_matrix}
#molinfo top set {center center_matrix rotate_matrix scale_matrix global_matrix} {{19.241970 17.340620 17.937765} {{1 0 0 -19.242} {0 1 0 -17.3406} {0 0 1 -17.9378} {0 0 0 1}} {{1 0 0 0} {0 1 0 0} {0 0 1 0} {0 0 0 1}} {{0.0387721 0 0 0} {0 0.0387721 0 0} {0 0 0.0387721 0} {0 0 0 1}} {{1 0 0 0} {0 1 0 0} {0 0 1 0} {0 0 0 1}}}
#scale by 1.9

# For borders
#translate to 0 0.95 0
#display height 1.5

track_particles 0 $diams $mat $scale_min $scale_max 0 0 $ntrack $ids
#vel_nc_poly_cut 0 $diams $mat $scale_min $scale_max 0 0
#gray_spheres_poly 0 $diams $mat

gen_render 0 -1 1 $rend $fmt
