# NOTE: fppvmd and batch need to be input

# Parse command-line arguments
if {$argc != 6} {
    puts "USAGE: vmd -args FPPVMD BATCH -dispdev none -eofexit < sheared_movie.tcl"
    exit 1
}
set fppvmdorder [lindex $argv 0]
set batch [lindex $argv 1]

package require pbctools

set mat AOShiny
color scale method RGB

#set diams "0.624025147 0.658693211 0.693361274 0.728029338 0.762697402"
set diams "1.0"
set scale_min 0.29
set scale_max 0.5
set rend TachyonInternal
set fmt [concat [format "%05d" $batch]-%05d-crystal.tga]

mol new $fppvmdorder waitfor all

light 0 off
light 1 on
display ambientocclusion on
display aoambient 0.80
display aodirect 0.60
display shadows on
display resize 1000 1000
display projection orthographic
display depthcue off
axes location off

pbc wrap -all -orthorhombic
# once more for good luck
pbc wrap -all -orthorhombic
display resetview
#rotate y by 90
scale by 1.9
molinfo top get {center center_matrix rotate_matrix scale_matrix global_matrix view_matrix}

set molnoshow [atomselect top "(vx*vx+vy*vy+vz*vz)**0.5 < 0.29"]
set selnum [$molnoshow num]
puts "Number of non-crystalline particles is $selnum"
$molnoshow set radius 0.01

vel_q6_poly_cut 0 $diams $mat $scale_min $scale_max 0 0

gen_render 0 -1 1 $rend $fmt
