# NOTE: fppvmd and batch need to be input

# Parse command-line arguments
if {$argc != 6} {
    puts "USAGE: vmd -args FPPVMD BATCH -dispdev none -eofexit < sheared_movie.tcl"
    exit 1
}
set fppvmd [lindex $argv 0]
set batch [lindex $argv 1]

package require pbctools

set mat AOShiny
set diams "0.9 0.95 1.0 1.05 1.1"
set scale_min 0
set scale_max 14
set rend TachyonInternal
set fmt [concat [format "%05d" $batch]-%05d.tga]

mol new $fppvmd waitfor all

light 0 off
light 1 on
display ambientocclusion on
display aoambient 0.80
display aodirect 0.60
display shadows on
display resize 1000 1000
display projection orthographic
display depthcue off
axes location off

# get the unit cell parameters of the current frame
#set cell [pbc get -now]
set sel [atomselect top "index 0"]
$sel get {z}
pbc wrap -all -orthorhombic 
# once more for good luck
pbc wrap -all -orthorhombic
set sel [atomselect top "index 0"]
$sel get {z}

#molinfo top get {center_matrix rotate_matrix scale_matrix global_matrix}

display resetview
molinfo top get {center_matrix rotate_matrix scale_matrix global_matrix}
scale by 1.9
#set A [atomselect top all]
#set minus_com [vecsub {0.0 0.0 0.0} {0.0 0.0 $sel}]
#$A moveby $minus_com
molinfo top get {center_matrix rotate_matrix scale_matrix global_matrix}
molinfo top set {{1 0 0 -62.727} {0 1 0 -62.6251} {0 0 1 -62.6582} {0 0 0 1}} {{1 0 0 0} {0 1 0 0} {0 0 1 0} {0 0 0 1}} {{0.0119599 0 0 0} {0 0.0119599 0 0} {0 0 0.0119599 0} {0 0 0 1}} {{1 0 0 0} {0 1 0 0} {0 0 1 0} {0 0 0 1}}
#molinfo top set {center_matrix rotate_matrix scale_matrix} {{{1 0 0 -62.736} {0 1 0 -62.6653} {0 0 1 -62.6934} {0 0 0 1}} {{1 0 0 0} {0 -1.83697e-16 1 0} {0 -1 -1.83697e-16 0} {0 0 0 1}} {{0.0226792 0 0 0} {0 0.0226792 0 0} {0 0 0.0226792 0} {0 0 0 1}}}

vel_nc_poly_cut 0 $diams $mat $scale_min $scale_max 0 0
#gray_spheres_poly 0 $diams $mat

gen_render 0 -1 1 $rend $fmt
