# NOTE: fppvmd and batch need to be input

# Parse command-line arguments
if {$argc != 6} {
    puts "USAGE: vmd -args FPPVMD BATCH -dispdev none -eofexit < stampede_movietcl"
    exit 1
}
set fppvmd [lindex $argv 0]
set batch [lindex $argv 1]

package require pbctools

set mat AOShiny
set diams "0.9 0.95 1.0 1.05 1.1"
set scale_min 0
set scale_max 14
set shave 0.6
set rend TachyonInternal
set fmt [concat [format "%05d" $batch]-%05d.tga]

mol new $fppvmd waitfor all

default_start
axes location off

vel_nc_poly_cut 0 $diams $mat $scale_min $scale_max 2 $shave

light 0 off
light 1 on
display ambientocclusion on
display aoambient 0.80
display aodirect 0.60
display shadows on


gen_render 0 -1 1 $rend $fmt
