pbc wrap -all
display projection perspective
display resetview
rotate y by 45
rotate x by 40
display distance 0
display height 2.8
translate by 0 0.2 0
color change rgb 7 0.0 0.6875 0.3125;# green
pbc box -color green
set diams {0.90 0.95 1.00 1.05 1.10}
user_poly 0 $diams AOShiny
display ambientocclusion on
display shadows on
display aoambient 1
light 0 off
light 1 off
animate goto 3
render TachyonInternal shiny.tga display %s

# For borders
translate to 0 0.95 0
display height 1.5
