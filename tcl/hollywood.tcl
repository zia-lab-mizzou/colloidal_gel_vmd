# hollywood.tcl
# 
# VMD movie generating script used for ASD; need to generate LAMMPS trajectory files from ASD output
# Also use Velocity as color scheme, which requires ASD to output Nc under vx
# Since ASD uses radius as the length unit, and LAMMPS uses diameter as the length unit
#   the diams below will be 2.0 (can vary around 2.0 if size jump is used)
package require pbctools

set imagestep 0
set image_diff 0 
set tdur 501300 
set tinc 1
set tstart 501300
set numsnaps [expr int($tdur/$tinc)+1]
puts "numsnaps $numsnaps"
set digits [expr int(log10($numsnaps-1))+1]
set first_snap 1
for {set j 0} {$j < $numsnaps} {incr j} {
set timestep [expr $tstart+$j*$tinc]
set imagestep [expr $j +$image_diff]
puts "timestep $timestep"

set snap Test_gel_vmd_${timestep}.lammpstrj 
puts "snap $snap"
# change filename
if {$first_snap} {
  puts "first step $j"
  set mol [mol new $snap]
  set first_snap 0
} else {
  puts "not first step but $j"
  animate read lammpstrj $snap $mol
  animate delete beg 0 end 0 skip 0 $mol
}

light 0 off
light 1 on
display ambientocclusion on
display aoambient 0.80
display aodirect 0.60
display shadows on
display resize 1000 1000
display projection orthographic
display depthcue off
color scale method RGB
axes location off

pbc wrap -all -orthorhombic
# once more for good luck
 pbc wrap -all -orthorhombic
 display resetview
 scale by 1.9
puts "just scaled"
 set material AOShiny
 set diams "2.07700" 
# change particle diameter
 set scale_min 0
 set scale_max 1
 set axis 0
 set shave 0
puts "mol $mol" 
set numtypes 1
#[get_num_types $mol]
 if {[llength $diams] != $numtypes} {
 puts "Need to supply values for $numtypes types"
 return
   }
# repclear $mol
   for {set ii 1} {$ii <= $numtypes} {incr ii} {
    mol addrep $mol
     set rep_number [expr $ii-1]
     set diam [expr 0.33*[lindex $diams [expr $ii-1]]]
     if {$axis == 0} {
     set saxis x
     } elseif {$axis == 1} {
     set saxis y
     } elseif {$axis == 2} {
     set saxis z
     } else {
     puts "axis needs to be 0, 1, or 2"
     return
     }
     set cell [molinfo $mol get {a b c}]
     set plane [expr $shave*[lindex $cell $axis]]
     mol modselect $rep_number $mol "type $ii && $saxis > $plane"
     mol modstyle $rep_number $mol VDW $diam 12
     mol modmaterial $rep_number $mol $material
     mol modcolor $rep_number $mol Velocity
     mol colupdate $rep_number $mol on
     mol selupdate $rep_number $mol on
     mol scaleminmax $mol $rep_number $scale_min $scale_max
     }

if { $imagestep < 10} {
     set img_name [format "ASD-56-500q6_000$imagestep.tga"]} else {
if { $imagestep < 100} {
     set img_name [format "ASD-56-500q6_00$imagestep.tga"]} else {
if { $imagestep < 1000} {
     set img_name [format "ASD-56-500q6_0$imagestep.tga"]} else {
if { $imagestep < 10000} {
     set img_name [format "ASD-56-500q6_$imagestep.tga"]}
} } }



puts "image name $img_name"
    # you can set to whatever format.
render TachyonInternal $img_name
                                                                                                                }
