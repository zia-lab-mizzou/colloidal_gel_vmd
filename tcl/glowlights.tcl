##
## Example script to append glow lights to a VMD/Tachyon scene
##

##
## Distance to light = d
## Attenuation factor = 1 / (Kc + Kld + Kq(d^2)
##   Kc = constant attenuation factor
##   Kl = linear attenuation factor
##   Kq = quadratic attenuation factor
##

namespace eval ::GlowLights:: {
  variable numlights     ;# number of lights in lists
  variable positions     ;# list of light positions (X, Y, Z)
  variable attenuations  ;# list of light attenuation coefficient triplets:
                          # (Kc, Kl, Kq) 
  variable colors        ;# list of light colors (string names or indices)
  variable radii         ;# list of light radii, if visible
}

proc ::GlowLights::reset {} {
  variable numlights
  variable positions
  variable attenuations
  variable colors
  variable radii

  puts "Resetting light state..."

  set numlights 0
  set positions {}
  set attenuations {}
  set colors {}
  set radii {}
}

proc ::GlowLights::new_light { pos col atten rad  } {
  variable numlights
  variable positions
  variable attenuations
  variable colors
  variable radii

#  puts "New glow light, pos: $pos color: $col attenuation: $atten rad: $rad"

  incr numlights
  lappend positions $pos
  lappend attenuations $atten
  lappend colors $col
  lappend radii $rad
}

proc ::GlowLights::draw_lights {} {
  variable numlights
  variable positions
  variable attenuations
  variable colors
  variable radii

  draw delete all
#  puts "drawing lights in new molecule..."
#  mol new 
  
  for {set i 0} {$i < $numlights} {incr i} {
    draw color   [lindex $colors $i]
    draw sphere  [lindex $positions $i] radius [lindex $radii $i] resolution 16
  }
}

proc ::GlowLights::print_stats {} {
  variable numlights
  variable positions
  variable attenuations
  variable colors
  variable radii

  puts "$numlights glow lights defined" 
  for {set i 0} {$i < $numlights} {incr i} {
    puts [format "glowlight\[%d\] pos: %s color: %s atten: %s rad: %s" \
                  $i [lindex $positions $i] \
                     [lindex $colors $i] \
                     [lindex $attenuations $i] \
                     [lindex $radii $i]]
  }
}

proc ::GlowLights::append_to_scene { filename } {
  variable numlights
  variable positions
  variable attenuations
  variable colors
  variable radii

  puts "Appending glow lights to Tachyon scene file"
  set fd [open $filename "a+"]

  # seek to 10 chars back from the end..
  seek $fd -11 end 

  puts $fd ""
  puts $fd "#"
  puts $fd "# VMD Beginning of glow lights"
  puts $fd "#"
  puts $fd ""

  # negate Z direction for Tachyon
  set ZN [list {1.0 0.0 0.0 0.0} {0.0 1.0 0.0 0.0} {0.0 0.0 -1.0 0.0} {0.0 0.0 0.0 1.0}]

  set R [lindex [molinfo top get rotate_matrix] 0]
  set S [lindex [molinfo top get scale_matrix] 0]
  set C [lindex [molinfo top get center_matrix ] 0]
  set T [lindex [molinfo top get global_matrix ] 0]
  set t [transoffset {0 0 -2}]
  set it [measure inverse $t]
  set G [transmult $ZN $S $R $T $C ]

  for {set i 0} {$i < $numlights} {incr i} {
    set lightpos [lindex $positions $i]
#    set translightpos $lightpos
    set translightpos [coordtrans $G $lightpos]

    puts $fd [format "# Glow light\[%d\]" $i]
    puts $fd "Light "
    puts $fd "  Center [format "%.5f %.5f %.5f" [lindex $translightpos 0] [lindex $translightpos 1] [lindex $translightpos 2]]"
#    puts $fd "  Rad [lindex $radii $i]"
    puts $fd "  Rad 0.0"
    puts $fd "  Attenuation constant [lindex $attenuations $i 0] linear [lindex $attenuations $i 1] quadratic [lindex $attenuations $i 2]"
    puts $fd "  Color [colorinfo rgb [lindex $colors $i]]"
  }

  puts $fd ""
  puts $fd "End_Scene"
 
  close $fd
}

proc ::GlowLights::render_scene { scenefilename imgfilename } {
  global env

  puts "Rendering scene" 

  render Tachyon $scenefilename
  append_to_scene $scenefilename

  # set platform-specific executable suffix
  set archexe ""
  switch [vmdinfo arch] {
    WIN64 -
    WIN32 {
      set archexe ".exe"
    }
  }

  set tachyonexe [format "tachyon%s" $archexe];
  set tachyoncmd \
    [::ExecTool::find -interactive -description "Tachyon Ray Tracer" \
      -path [file join $env(VMDDIR) \
      "tachyon_[vmdinfo arch]$archexe"] $tachyonexe]

  if {$tachyoncmd == {}} {
    puts "Cannot find Tachyon, aborting"
  }

  set rendercmd [ format "\"%s\"" $tachyoncmd]
#  set arglist "-mediumshade $scenefilename -format Targa -aasamples 4 -trans_vmd -o $imgfilename"
#  set rendercmd [concat $rendercmd $arglist]
  exec $tachyoncmd -mediumshade $scenefilename -format Targa -aasamples 4 -trans_vmd -o $imgfilename
}

proc ::GlowLights::selftest {} {
  # reset light state by default
  ::GlowLights::reset
  ::GlowLights::print_stats
  ::GlowLights::new_light { 0.0 0.0 0.0 } white { 1.0 0.2 0.05 } 0.1 
  ::GlowLights::new_light { 1.0 0.0 0.0 } red   { 1.0 0.2 0.05 } 0.1 
#  ::GlowLights::new_light { 0.0 1.0 0.0 } green { 1.0 0.2 0.05 } 0.1 
#  ::GlowLights::new_light { 0.0 0.0 1.0 } blue  { 1.0 0.2 0.05 } 0.1 

  ::GlowLights::print_stats

#  ::GlowLights::draw_lights

  ::GlowLights::render_scene /tmp/foo.dat /tmp/foo.tga
}

#::GlowLights::selftest

