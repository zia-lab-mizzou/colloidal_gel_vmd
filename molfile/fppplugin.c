#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "molfile_plugin.h"
#include "hdf5.h"
#include "hdf5_hl.h"
#include <math.h>
#include "vmdconio.h"
#include <unistd.h>
#include "inttypes.h"

#define MAX_STR_LEN 80
#define MAX_NUM_H5_FILES 16384

#ifndef M_PI_2
#define M_PI_2 1.57079632679489661922
#endif

#ifndef MIN
#define MIN(A,B) ((A) < (B) ? (A) : (B))
#endif

#ifndef MAX
#define MAX(A,B) ((A) > (B) ? (A) : (B))
#endif

#define INV_RT_3 0.577350269

static molfile_plugin_t plugin;

/* ---------------------------------------------------------------------- */

typedef struct {

  // Particle and box variables

  int natoms;                          // # of atoms
  int ntypes;                          // # of atom types (sizes for me)
  float xlo, xhi, ylo, yhi, zlo, zhi;  // box boundaries
  float xy, xz, yz;                    // might have to make these and above
                                       // dynamic
  float *sizes;                        // diameter for each atom type

  // Snapshot and HDF5 variables

  int first_snap;  // first snapshot to read, numbered from zero!
                   // the fppvmd file numbers from 1
  int nsnaps;      // total # of snapshots to open in VMD

  // Variables about the HDF5 library

  int nfiles;                         // # of HDF5 files
  int file_nsnaps[MAX_NUM_H5_FILES];  // # of snaps in each file
  int tot_file_nsnaps;                // total # of snapshots available
  char base_path[MAX_STR_LEN];        // folder containing HDF5 files
  char file_rel_paths[MAX_NUM_H5_FILES][MAX_STR_LEN];  // relative paths

  // Stored HDF5 variables used in read_fpp_timestep

  int nreads;          // counter for # of reads so far
  int last_file;       // my index of last HDF5 file read
  hid_t h5_file;       // HDF5 id of HDF5 file
  hid_t h5_dataset;    // HDF5 id of position dataset in HDF5 file
  hid_t h5_dataspace;  // HDF5 id of dataspace of dataset
  hid_t h5_memspace;   // HDF5 id of dataspace of memory

  // Trajectory buffers with data to send to VMD

  float *data_xyz;  // atom coordinate array to hold single snapshot

#if vmdplugin_ABIVERSION > 10
  int *data_nc;   // contact numbers that get read into the velocity fields in vmd
  hid_t h5_file_nc;
  hid_t h5_dataset_nc;
  hid_t h5_dataspace_nc;
  hid_t h5_memspace_nc;
  molfile_timestep_metadata_t ts_meta;
#endif

  float **data_box; // changing triclinic box dimensions
} fppdata;

/* ---------------------------------------------------------------------- */

static int which_h5_snap(fppdata *data, int snap, int *h5id, int *snapid)
{
  // Return the indices of the HDF5 file and the snapshot within the file
  // Here, snaps run from 0, UNLIKE in the fppvmd files, which run from 1

  int tot_file_nsnaps = data->tot_file_nsnaps;
  int nfiles = data->nfiles;
  int *file_nsnaps = data->file_nsnaps;
  int i;

  if ((snap < 0) || (snap >= tot_file_nsnaps)) {
    vmdcon_printf(VMDCON_ERROR, "fppplugin) bad snap index %d\n", snap);
    return -1;
  }

  for (i = 0; i < nfiles; i++) {
    if (snap < file_nsnaps[i]) {
      *h5id = i;
      *snapid = snap;
      return 0;
    }
    snap -= file_nsnaps[i];
  }
  return -1;
}

/* ---------------------------------------------------------------------- */

static void *open_fpp_read(const char *filename, const char *filetype, int *natoms)
{
  char buf[MAX_STR_LEN],particle_path[MAX_STR_LEN],paths_path[MAX_STR_LEN];
  FILE *fp;
  int i,j,first_snap,nsnaps;
  fppdata *data;
  int tinit[MAX_NUM_H5_FILES];
  int tfini[MAX_NUM_H5_FILES];
  int tincr[MAX_NUM_H5_FILES];
  int last_tinit;

  fp = fopen(filename, "r");
  if (!fp) return NULL;

  // Get fppvmd data ready

  data = (fppdata *) calloc(1, sizeof(fppdata));
  data->nreads = 0;
  data->last_file = -1;
  *natoms = 0;
  data->data_xyz = NULL;
  data->sizes = NULL;
#if vmdplugin_ABIVERSION > 10
  data->data_nc = NULL;
#endif
  data->data_box = NULL;

  // Get the base path from the input and the range of snaps to read

  
  fgets(buf, MAX_STR_LEN, fp);  // ITEM: DATA DIRECTORY (WITH TERMINAL SLASH)
  fgets(data->base_path, MAX_STR_LEN, fp);
  data->base_path[strlen(data->base_path)-1] = '\0';
  fgets(buf, MAX_STR_LEN, fp);  // ITEM: first_snap nsnaps
  fscanf(fp, "%d %d\n", &first_snap, &nsnaps);
  first_snap -= 1;  // to make snaps start from 0 instead of 1
  data->first_snap = first_snap;
  data->nsnaps = nsnaps;
  fclose(fp);

  // Open particles.txt just to get the number of particles for now

  vmdcon_printf(VMDCON_INFO, data->base_path);
  sprintf(particle_path, "%s%s", data->base_path, "particle.txt");
  vmdcon_printf(VMDCON_INFO,
		"fppplugin) opening particle.txt\n");
  vmdcon_printf(VMDCON_INFO, particle_path);
  fp = fopen(particle_path, "r");
  fgets(buf, MAX_STR_LEN, fp);  // ITEM: NUMBER OF PARTICLES
  fscanf(fp, "%d\n", &(data->natoms));
  *natoms = data->natoms;
  fclose(fp);

  // Go through paths.txt to grab filenames and timestep info

  vmdcon_printf(VMDCON_INFO,
		"fppplugin) opening paths.txt\n");
  sprintf(paths_path, "%s%s", data->base_path, "paths.txt");
  vmdcon_printf(VMDCON_INFO, paths_path);
  fp = fopen(paths_path, "r");
  fgets(buf, MAX_STR_LEN, fp);
  j = 0;
  while (fscanf(fp, "%s %d %d %d\n", buf,
		&(tinit[j]),
		&(tfini[j]),
		&(tincr[j])) != EOF) {
    sprintf(data->file_rel_paths[j], "%s", buf);
    j++;
    if (j == MAX_NUM_H5_FILES) {
      printf("Too many H5 files!\n");
    }
  }
  data->nfiles = j;
  fclose(fp);

  // Get the number of snapshots in each file
  // Can allow for overlap, but need to check for this

  vmdcon_printf(VMDCON_INFO,
		"fppplugin) getting snapshots\n");
  data->tot_file_nsnaps = 0;
  for (i = data->nfiles - 1; i>=0; i--) {
    if (i == data->nfiles-1) {
      data->file_nsnaps[i] =
	(int) (tfini[i]-tinit[i])/tincr[i] + 1;
    } else {
      if (tfini[i] >= last_tinit) {
	data->file_nsnaps[i] =
	  (int) (last_tinit-tinit[i])/tincr[i] + 1;
      } else {
	data->file_nsnaps[i] =
	  (int) (tfini[i]-tinit[i])/tincr[i] + 1;
      }
    }
    last_tinit = tinit[i];
    data->tot_file_nsnaps += data->file_nsnaps[i];
  }

#if vmdplugin_ABIVERSION > 10

  // Check if there is a file for each HDF5 coordinate file read with .nc
  // Will read these contact numbers into velocity fields (hack)

  data->ts_meta.count = -1;
  data->ts_meta.has_velocities = 1;
  for (i = 0; i < data->nfiles; i++) {
    sprintf(buf, "%s%s.nc", data->base_path, data->file_rel_paths[i]);
    if (access(buf, F_OK) == -1) {
      data->ts_meta.has_velocities = 0;
    }
  }
  if (data->ts_meta.has_velocities == 1) {
    vmdcon_printf(VMDCON_INFO,
		  "fppplugin) found nc data ... importing as velocities\n");
  } else {
    vmdcon_printf(VMDCON_INFO,
		  "fppplugin) did not find nc data\n");
  }

#endif

  return data;
}

/* ---------------------------------------------------------------------- */

#if vmdplugin_ABIVERSION > 10
static int read_timestep_metadata(void *mydata,
				  molfile_timestep_metadata_t *meta)
{

  fppdata *data = (fppdata *) mydata;

  meta->count = -1;
  meta->has_velocities = data->ts_meta.has_velocities;

  return MOLFILE_SUCCESS;
}
#endif


/* ---------------------------------------------------------------------- */

/* DONE */
static int read_fpp_structure(void *mydata, int *optflags,
			      molfile_atom_t *atoms)
{
  molfile_atom_t *atom;
  fppdata *data = (fppdata *) mydata;
  char str[16];
  unsigned int type;
  FILE *fp;
  char buf[MAX_STR_LEN],particle_path[MAX_STR_LEN],box_path[MAX_STR_LEN];
  int i;

  *optflags = MOLFILE_MASS;

  // Open particle.txt
  // We already read the # of particles in open_fpp_read
  // Fill the number of types and the sizes

  sprintf(particle_path, "%s%s", data->base_path, "particle.txt");
  fp = fopen(particle_path, "r");
  fgets(buf, MAX_STR_LEN, fp);  // ITEM: NUMBER OF PARTICLES
  fgets(buf, MAX_STR_LEN, fp);
  fgets(buf, MAX_STR_LEN, fp);  // ITEM: NUMBER OF TYPES
  fscanf(fp, "%d\n", &(data->ntypes));
  data->sizes = (float *) malloc(data->ntypes*sizeof(float));
  fgets(buf, MAX_STR_LEN, fp);  // ITEM: SIZES IN TYPE ORDER
  for (i = 0; i < data->ntypes; i++) {
    fscanf(fp, "%f\n", &(data->sizes[i]));
  }

  // Set the atom dummy info as you read the types in atom order

  fgets(buf, MAX_STR_LEN, fp);  // ITEM: LIST OF TYPES IN LAMMPS ATOM ORDER
  for (i = 0; i < data->natoms; i++) {
    atom = atoms + i;

    fscanf(fp, "%u\n", &type);
    sprintf(str,"%u",type);
    strncpy(atom->type,str,sizeof(str));

    // Dummy settings

    atom->mass = 1.0;
    atom->resid = 0;
    strncpy(atom->name,"",1);
    strncpy(atom->resname,"",1);
    strncpy(atom->chain,"",1);
    strncpy(atom->segid,"",1);
  }

  // Don't load velocities for now

#if vmd_plugin_ABIVERSION > 10
  data->ts_meta.count = -1;
  data->ts_meta.has_velocities = 0;
#endif

  fclose(fp);

  // Get time-dependent box bounds from box.txt
  // Ignore the first column containing the timestep

  // dummy buffer for timestep
  int step;

  int tot_file_nsnaps = data->tot_file_nsnaps;
  data->data_box = (float **) malloc(tot_file_nsnaps*sizeof(float *));
  for (i = 0; i < tot_file_nsnaps; i++) {
    (data->data_box)[i] = (float *) malloc(9*sizeof(float));
  }
  sprintf(box_path, "%s%s", data->base_path, "box.txt");
  fp = fopen(box_path, "r");
  fgets(buf, MAX_STR_LEN, fp);  // ITEM: xlo_bound xhi_bound xy ylo_bound
                                // yhi_bound xz zlo_bound zhi_bound yz
  for (i=0; i < tot_file_nsnaps; i++) {
    fscanf(fp, "%d %f %f %f %f %f %f %f %f %f\n",
	   &step,
	   &(data->data_box[i][0]),
	   &(data->data_box[i][1]),
	   &(data->data_box[i][2]),
	   &(data->data_box[i][3]),
	   &(data->data_box[i][4]),
	   &(data->data_box[i][5]),
	   &(data->data_box[i][6]),
	   &(data->data_box[i][7]),
	   &(data->data_box[i][8]));
  }

  fclose(fp);
  return MOLFILE_SUCCESS;
}

/* ---------------------------------------------------------------------- */

static void allocate_buffers(fppdata *mydata)
{
  fppdata *data = (fppdata *) mydata;
  int natoms = data->natoms;

  // Single snapshot buffer

  float *data_xyz = malloc(3*natoms*sizeof(float));
  data->data_xyz = data_xyz;

#if vmdplugin_ABIVERSION > 10
  int *data_nc;
  if (data->ts_meta.has_velocities) {
    data_nc = malloc(natoms*sizeof(int));
    data->data_nc = data_nc;
  }
#endif
}

/* ---------------------------------------------------------------------- */

static float cosangle2deg(float val)
{
  // Convert cosine of angle to degrees.
  // Bracket within -1.0 <= x <= 1.0 to avoid NaNs due to rounding errors.

  if (val < -1.0) val = -1.0;
  if (val >  1.0) val =  1.0;
  return (float) (90.0 - asin(val)*90.0/M_PI_2);
}

/* ---------------------------------------------------------------------- */

static int read_fpp_timestep(void *mydata, int natoms,
			     molfile_timestep_t *ts)
{
  fppdata *data = (fppdata *) mydata;
  int whs_err;
  int h5id,snapid;
  char fname[MAX_STR_LEN];
  int last_file = data->last_file;
  hsize_t start[3] = { 0, 0, 0 };
  hsize_t start_nc[2] = { 0, 0 };
  hsize_t ct_pos[3] = { 1, data->natoms, 3 };
  hsize_t ct_nc[2] = { 1, data->natoms };
  int i,j,nc;
  float ylohi;
  float vel; // dummy velocity used to get linear coloring with nc

  // Check if we should read or skip this step

  if (data->nreads == data->nsnaps) return MOLFILE_ERROR;
  if (!ts) return MOLFILE_SUCCESS;

  if (data->data_xyz == NULL) {
    allocate_buffers(data);
  }

  whs_err = which_h5_snap(data, data->first_snap+data->nreads, &h5id, &snapid);

  if (whs_err == -1) {
    vmdcon_printf(VMDCON_ERROR,
		  "fppplugin) unable to determine which H5 file.\n");
    return MOLFILE_ERROR;
  }

  if (h5id != last_file) {
    if (data->nreads > 0) {
      H5Sclose(data->h5_dataspace);
      H5Sclose(data->h5_memspace);
      H5Dclose(data->h5_dataset);
      H5Fclose(data->h5_file);
#if vmdplugin_ABIVERSION > 10
      if (data->ts_meta.has_velocities) {
	H5Sclose(data->h5_dataspace_nc);
	H5Sclose(data->h5_memspace_nc);
	H5Dclose(data->h5_dataset_nc);
	H5Fclose(data->h5_file_nc);
      }
#endif
    }
    sprintf(fname, "%s%s", data->base_path, data->file_rel_paths[h5id]);
    data->h5_file = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    data->h5_dataset = H5Dopen(data->h5_file, "pos", H5P_DEFAULT);
    data->h5_dataspace = H5Dget_space(data->h5_dataset);
    data->h5_memspace = H5Screate_simple(3, ct_pos, NULL);
#if vmdplugin_ABIVERSION > 10
    if (data->ts_meta.has_velocities) {
      sprintf(fname, "%s%s.nc", data->base_path, data->file_rel_paths[h5id]);
      data->h5_file_nc = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
      data->h5_dataset_nc = H5Dopen(data->h5_file_nc, "nc", H5P_DEFAULT);
      data->h5_dataspace_nc = H5Dget_space(data->h5_dataset_nc);
      data->h5_memspace_nc = H5Screate_simple(2, ct_nc, NULL);
    }
#endif
  }

  // Select starting position of hyperslab selection (contiguous data)

  for (i=0; i<3*data->natoms; i++) data->data_xyz[i] = 0.0;

  printf("%d %d\n", h5id, snapid);
  start[0] = snapid;
  H5Sselect_hyperslab(data->h5_dataspace, H5S_SELECT_SET, start, NULL,
		      ct_pos, NULL);
  H5Dread(data->h5_dataset, H5T_NATIVE_FLOAT, data->h5_memspace,
	  data->h5_dataspace, H5P_DEFAULT, data->data_xyz);
#if vmdplugin_ABIVERSION > 10
  if (data->ts_meta.has_velocities) {
    for (i=0; i<data->natoms; i++) data->data_nc[i] = 0;
    start_nc[0] = snapid;
    H5Sselect_hyperslab(data->h5_dataspace_nc, H5S_SELECT_SET, start_nc, NULL,
			ct_nc, NULL);
    H5Dread(data->h5_dataset_nc, H5T_NATIVE_INT, data->h5_memspace_nc,
	    data->h5_dataspace_nc, H5P_DEFAULT, data->data_nc);
  }
#endif

  // Always using triclinic!

  float xlo,xhi,xy,ylo,yhi,xz,zlo,zhi,yz;
  float xdelta;
  int isnap = data->first_snap+data->nreads;

  xlo = data->data_box[isnap][0];
  xhi = data->data_box[isnap][1];
  xy  = data->data_box[isnap][2];
  ylo = data->data_box[isnap][3];
  yhi = data->data_box[isnap][4];
  xz  = data->data_box[isnap][5];
  zlo = data->data_box[isnap][6];
  zhi = data->data_box[isnap][7];
  yz  = data->data_box[isnap][8];

  // Apply corrections to get these to work with VMD, as LAMMPS plugin does

  xdelta = MIN(0.0f,xy);
  xdelta = MIN(xdelta,xz);
  xdelta = MIN(xdelta,xy+xz);
  xlo -= xdelta;

  xdelta = MAX(0.0f,xy);
  xdelta = MAX(xdelta,xz);
  xdelta = MAX(xdelta,xy+xz);
  xhi -= xdelta;

  ylo -= MIN(0.0f,yz);
  yhi -= MAX(0.0f,yz);

  // Convert bounding box info to real box lengths

  ts->A = xhi - xlo;
  ylohi = yhi - ylo;
  ts->B = sqrt(ylohi*ylohi + xy*xy);
  ts->C = sqrt((zhi-zlo)*(zhi-zlo) + xz*xz + yz*yz);
  ts->alpha = cosangle2deg((xy*xz + ylohi*yz)/(ts->B * ts->C));
  ts->beta  = cosangle2deg(xz/ts->C);
  ts->gamma = cosangle2deg(xy/ts->B);

#if vmdplugin_ABIVERSION > 10
  // Initialize velocities to zero if present
  if (ts->velocities != NULL) memset(ts->velocities,0,3*natoms*sizeof(float));
#endif

  for (i = 0; i < natoms; i++) {
    j = 3 * i;
    ts->coords[j    ] = data->data_xyz[j    ];
    ts->coords[j + 1] = data->data_xyz[j + 1];
    ts->coords[j + 2] = data->data_xyz[j + 2];
  }

#if vmdplugin_ABIVERSION > 10
  if (data->ts_meta.has_velocities) {
    if (ts->velocities != NULL) {
      for (i = 0; i < natoms; i++) {
	j = 3 * i;
	nc = data->data_nc[i];
	vel = (float) nc;
	vel *= INV_RT_3;
	ts->velocities[j    ] = vel;
	ts->velocities[j + 1] = vel;
	ts->velocities[j + 2] = vel;
      }
    }
  }
#endif

  data->nreads = data->nreads + 1;
  last_file = h5id;
  return MOLFILE_SUCCESS;
}

/* ---------------------------------------------------------------------- */

static void close_fpp_read(void *mydata)
{
  fppdata *data = (fppdata *)mydata;
  int nreads = data->nreads;

  // Coordinate data

  if (nreads > 0) {
    H5Sclose(data->h5_dataspace);
    H5Sclose(data->h5_memspace);
    H5Dclose(data->h5_dataset);
    H5Fclose(data->h5_file);
  }
  free(data->data_xyz);

  // Nc data

#if vmdplugin_ABIVERSION > 10
  if (data->ts_meta.has_velocities) {
    if (nreads > 0) {
      H5Sclose(data->h5_dataspace_nc);
      H5Sclose(data->h5_memspace_nc);
      H5Dclose(data->h5_dataset_nc);
      H5Fclose(data->h5_file_nc);
    }
    free(data->data_nc);
  }
#endif

  // These get allocated during read_fpp_structure

  free(data->sizes);
  for (int i=0; i<data->tot_file_nsnaps; i++) {
    free(data->data_box[i]);
  }
  free(data->data_box);
}

/* ---------------------------------------------------------------------- */

VMDPLUGIN_API int VMDPLUGIN_init()
{
  memset(&plugin, 0, sizeof(molfile_plugin_t));
  plugin.abiversion = vmdplugin_ABIVERSION;
  plugin.type = MOLFILE_PLUGIN_TYPE;
  plugin.name = "fpostproc_nc";
  plugin.prettyname = "fpostproc_nc";
  plugin.author = "Ben Landrum";
  plugin.majorv = 0;
  plugin.minorv = 0;
  plugin.is_reentrant = VMDPLUGIN_THREADUNSAFE;
  plugin.filename_extension = "fppvmd";
  plugin.open_file_read = open_fpp_read;
  plugin.read_structure = read_fpp_structure;
  plugin.read_next_timestep = read_fpp_timestep;
  plugin.read_timestep_metadata = read_timestep_metadata;
  plugin.close_file_read = close_fpp_read;
  return VMDPLUGIN_SUCCESS;
}

/* ---------------------------------------------------------------------- */

VMDPLUGIN_API int VMDPLUGIN_register(void *v, vmdplugin_register_cb cb)
{
  (*cb)(v, (vmdplugin_t *) &plugin);
  return VMDPLUGIN_SUCCESS;
}

/* ---------------------------------------------------------------------- */

VMDPLUGIN_API int VMDPLUGIN_fini()
{
  return VMDPLUGIN_SUCCESS;
}

/* ---------------------------------------------------------------------- */

#ifdef TEST_PLUGIN

int main(int argc, char *argv[]) {
  molfile_timestep_t timestep;
  molfile_atom_t *atoms = NULL;
  void *v;
  int natoms;
  int i, j, opts;
#if vmdplugin_ABIVERSION > 10
  molfile_timestep_metadata_t ts_meta;
#endif

  // Test open_fpp_read

  v = open_fpp_read(argv[1], "fpp", &natoms);
  if (!v) {
    fprintf(stderr, "open_fpp_read failed for file %s\n", argv[1]);
    close_fpp_read(v);
    return 1;
  }
  fprintf(stderr, "open_fpp_read succeeded for file %s\n", argv[1]);
  fprintf(stderr, "number of atoms: %d\n", natoms);

  // Test read_fpp_structure

  timestep.coords = (float *) malloc(3*sizeof(float)*natoms);
  atoms = (molfile_atom_t *) malloc(sizeof(molfile_atom_t)*natoms);

  if ((read_fpp_structure(v, &opts, atoms) == MOLFILE_ERROR)) {
    fprintf(stderr, "read_fpp_structure failed for file %s\n", argv[1]);
    close_fpp_read(v);
    return 1;
  }
  fprintf(stderr, "read_fpp_structure: options=0x%08x\n", opts);

  // Test read_timestep_metadata

  // BJL NEED THIS FOR BOX?

#if vmdplugin_ABIVERSION > 10
  read_timestep_metadata(v, &ts_meta);
  if (ts_meta.has_velocities) {
    fprintf(stderr, "found timestep velocities (nc) metadata.\n");
  }
  timestep.velocities = (float *) malloc(3*natoms*sizeof(float));
#endif

  // Test read_fpp_timestep and nc if they are loaded

  // BJL NEED BOX HERE?

  j = 0;
  while (!read_fpp_timestep(v, natoms, &timestep)) {
    fprintf(stderr, "box dimensions:\n");
    fprintf(stderr, "A/B/C ' %.3f %.3f %.3f\n",
	    timestep.A, timestep.B, timestep.C);
    fprintf(stderr, "alpha/beta/gamma ' %.3f %.3f %.3f\n",
	    timestep.alpha, timestep.beta, timestep.gamma);
    for (i = 0; i < 10; i++) {
      fprintf(stderr, "atom %09d: type=%s, "
	      "x/y/z = %.3f %.3f %.3f "
#if vmdplugin_ABIVERSION > 10
	      "vx/vy/vz = %.3f %.3f %.3f "
#endif
	      "\n",
	      i, atoms[i].type,
	      timestep.coords[3*i  ],
	      timestep.coords[3*i+1],
	      timestep.coords[3*i+2]
#if vmdplugin_ABIVERSION > 10
	,timestep.velocities[3*i],
	timestep.velocities[3*i+1],
	timestep.velocities[3*i+2]
#endif
	      );
    }
    j++;
  }
  fprintf(stderr, "ended read_next_timestep on frame %d\n", j);

  close_fpp_read(v);

#if vmdplugin_ABIVERSION > 10
  free(timestep.velocities);
#endif
  free(timestep.coords);
  return 0;
}
#endif
