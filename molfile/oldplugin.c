#include "molfile_plugin.h"
#include "hdf5.h"
#include "hdf5_hl.h"

#define MAX_STRING_LEN 80

/*
  need to stop being lazy and start using the HDF5 stuff
    1) check types used in fpostproc
    2) make sure you are reading them correctly into data (read_position)
      a) get this to work with .fppvmd file
    3) make sure get_xyz works with above framework
    4) set the read_fpostproc_structure to work with .fppvmd files
*/

/* global, non-constant variables */
static int position_already_read = -1;
static int reads = 0;
static int current_time = 0;

typedef struct {
  int natoms;
  int ntime;

  char h5dir[MAX_STRING_LEN];
  char file_name[MAX_STRING_LEN];
  hid_t file_id;
  hid_t dataset_id;

  double ***data_xyz;
} fpostprocdata;

/* produces an x,y,z array from fpostprocdata */
/* DONE as long as data_xyz points to the right stuff */
static void get_xyz(void *mydata, int atom_nr, int time_i,
		    double xyz_array[3], double ***data_xyz) {
  fpostprocdata *data = (fpostprocdata *) mydata;

  xyz_array[0] = data_xyz[time_i][atom_nr][0];
  xyz_array[1] = data_xyz[time_i][atom_nr][1];
  xyz_array[2] = data_xyz[time_i][atom_nr][2];
}

static void *open_fpostproc_read(const char *filename, const char *filetype,
				 int *natoms) {

  FILE *fp;
  fpostprocdata *data;
  int ts_id_init, ts_id_fini;
  char buf[MAX_STRING_LEN];

  /* parse the *.fppvmd file */

  fp = fopen(filename, "r");
  if (!fp) return NULL;
  if (fgets(buf, MAX_STRING_LEN, fp) != NULL) {
    fscanf(fp, "%s\n", data.h5dir);
  }
  fscanf(fp, "%d %d\n", &ts_id_init, &ts_id_fini);
  data->ntime = ts_id_fini - ts_id_init + 1;
  fclose(fp);

  /* get natoms and box dims from box.txt */

  char path[MAX_STRING_LEN];
  sprintf(path, sizeof path, "%s%s", data->h5dir, "box.txt");
  fp = fopen(path, "r");
  if (!fp) return NULL;

  /* ... */
  data->natoms = natoms;



  /* get timestep numbers from paths.txt */
  /* assuming equally spaced timesteps, just need first timestep and tinc */


  
  




  /* anchor HDF5 */
  /* does this match fpostproc types? */
  hid_t file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
  hid_t dataset_id = H5Dopen(file_id, "NEED", H5P_DEFAULT);

  /* set data */
  data->file_name = filename;

  /* h5md specific */
  data->file_id = file_id;
  data->dataset_id=dataset_id;

  /* set according to h5md, but used also in lmp plugin */
  *natoms = data->natoms;



  return data;
}

static int read_fpostproc_structure(void *mydata, int *optflags,
				    molfile_atom_t *atoms) {
  molfile_atom_t *atom;
  fpostprocdata *data = (fpostprocdata *) mydata;

  /* not using any options ... all I have are types (masses?) */
  *optflags = MOLFILE_NOOPTIONS;

  int error_status_of_read = -1;

  /* need atom->type, atom->name?, atom->atomicnumber?, atom->mass?,
     segid, resid, */

}

/* seems to be done */
/* requires
   data->ntime
   data->data_xyz
   globals current_time and reads

   functions:
   read_position(data)
   get_xyz(data, i, current_time, xyz_array, data_xyz);
 */
static int read_fpostproc_timestep(void *mydata, int natoms,
				   molfile_timestep_t *ts) {

  int i;
  double xyz_array[3];
  float x, y, z;

  fpostprocdata *data = (fpostprocdata *) mydata;

  /* read position from HDF5 files */
  /* put this into data, to be transferred to VMD */
  if (position_already_read < 0) {
    read_position(data);
    position_already_read = 1;
  }

  double ***data_xyz = data->data_xyz;

  /* error if beyond the number of times in the file */
  unsigned int ntime = data->ntime;
  if (current_time >= ntime - 1) {
    return MOLFILE_ERROR;
  }

  /* read the coordinates only */
  for (i = 0; i < natoms; i++) {

    /* get current time from dividing # reads by # atoms */
    current_times = reads / natoms;
    reads += 1;

    get_xyz(data, i, current_time, xyz_array, data_xyz);
    x = xyz_array[0];
    y = xyz_array[1];
    z = xyz_array[2];

    if (ts != NULL) {
      ts->coords[3*i    ] = x;
      ts->coords[3*i + 1] = y;
      ts->coords[3*i + 2] = z;
    } else {
      /* TODO insert error here */
      break;
    }

  }
  /* this is from molfile */
  return MOLFILE_SUCCESS;
}

static void read_position(fpostprocdata *data) {

}

/*
  TODO:
    1) add file closing here
    2) free other allocated memory (do after decided on structure)
*/
static void close_fpostproc_read(void *mydata) {

  fpostprocdata *data = (fpostprocdata *) mydata;
  int i, j;

  /* INSERT some file closing function for *.fppvmd and maybe hdf5 */

  free(data->file_name);

  for (i = 0; i < data->ntime; i++) {
    for (j = 0; j < data->natoms; j++) {
      free(data->data_xyz[i][j]);
    }
    free(data->data_xyz[i]);
  }
  free(data->data_xyz);

  free(data);

  /* reset current_time, reads, and position_already_read */
  current_time = 0;
  reads = 0;
  position_already_read = -1;
}

/* registration stuff */
static molfile_plugin_t plugin;

VMDPLUGIN_API int VMDPLUGIN_init() {
  memset(&plugin, 0, sizeof(molfile_plugin_t));
  plugin.abiversion = vmdplugin_ABIVERSION;
  plugin.type = MOLFILE_PLUGIN_TYPE;
  plugin.name = "fpostproc";
  plugin.prettyname = "fpostproc";
  plugin.author = "Ben Landrum";
  plugin.majorv = 0;
  plugin.minorv = 0;
  plugin.is_reentrant = VMDPLUGIN_THREADUNSAFE;
  plugin.filename_extension = "fppvmd";
  plugin.open_file_read = open_fpostproc_read;
  plugin.read_structure = read_fpostproc_structure;
  plugin.read_next_timestep = read_fpostproc_timestep;
  plugin.close_file_read = close_fpostproc_read;

  /* lmp plugin has metadata stuff */
  /* h5md has bond stuff */

  return VMDPLUGIN_SUCCESS;
}

/* DONE */
VMDPLUGIN_API int VMDPLUGIN_register(void *v, vmdplugin_register_cb cb) {
  (*cb)(v, (vmdplugin_t *) &plugin);
  return VMDPLUGIN_SUCCESS;
}

/* DONE */
VMDPLUGIN_API int VMDPLUGIN_fini() {
  return VMDPLUGIN_SUCCESS;
}
